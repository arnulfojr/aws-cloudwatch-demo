#!/bin/bash

set -e

# get all our juicy stuff off the internet
if [ ! -d node_modules ]; then
  npm install && npm install --only-dev
fi

# build our lambda
npm run-script lambda:compile

# package it with SAM
sam build \
  --template lambda/template.yaml \
  --base-dir lambda/ \
  --manifest package.json \
  --build-dir build

# we now cdk build and deploy!
npm run-script cdk synthesize

if [ -z "${AWS_REGION}" ]; then
  echo "Please provide an AWS region 'AWS_REGION'"
  exit 1
fi

npm run-script cdk deploy

npm run-script clean
