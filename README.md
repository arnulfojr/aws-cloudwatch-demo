# Useful commands

make sure to have the awscli installed and configured with an account
https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html

and also install SAM (only used to package the app)
https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html


env variables required:

* `AWS_DEFAULT_REGION=us-east-1` or any other region
* `AWS_ACCOUNT_ID` your aws account ID
or:
* `CDK_DEFAULT_ACCOUNT`
* `CDK_DEFAULT_REGION`

```
npm run lambda:compile
npx cdk synth
npx cdk deploy
npx cdk destroy
```
