import { Context } from "aws-lambda";

export enum LogLevel {
  TRACE,
  INFO,
  WARNING,
  ERROR,
};

export interface Metric {
  Api: string;
  Service: string;
  AttemptCount: number;
  Latency: number;
  Params: string[];
}

export class Logger {
  private context?: Context;
  private level: LogLevel;

  constructor(level: LogLevel = LogLevel.TRACE) {
    this.level = level;
  }

  public setContext(context: Context) {
    this.context = context;
  }

  public getContext(): Context | undefined {
    return this.context;
  }

  public trace(message: string | object): void {
    if (this.level === LogLevel.TRACE) {
      this.log({ message });
    }
  }

  public metric(metric: Metric): void {
    this.log(metric);
  }

  private log(object: object): void {
    const content = {
      ...object,
      context: this.context,
    };

    console.log(JSON.stringify(content));
  }
}

const logger = new Logger();

export function initLogger(context: Context): Logger {
  logger.setContext(context);
  return logger;
}

export function getLogger(): Logger {
  return logger;
}
