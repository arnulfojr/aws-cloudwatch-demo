import { ScheduledEvent, Context } from "aws-lambda";
import { initLogger } from "./logger";
import { LogGenerator } from "./LogGenerator";

const logGenerator = new LogGenerator();

export async function handler(
  event: ScheduledEvent,
  context: Context
): Promise<void> {
  const logger = initLogger(context);
  logger.trace({
    event,
    context
  });

  await logGenerator
    .withLogger(logger)
    .withBaseDelay(50)
    .withAmountOfEntries(5_000)
    .generateRandomMetrics();
}
