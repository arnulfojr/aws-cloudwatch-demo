import { Logger, Metric } from "./logger";

const Services: string[] = ["DynamoDB", "S3", "Route53"];
const ApiCalls: string[] = ["GetItem", "ListBuckets", "DeleteItem"];
const Params: Array<Array<string>> = [
  ["a", "b", "c"],
  ["x", "y", "z"],
  ["1", "2", "3"]
];

function getRandomElementFrom<T>(models: Array<T>): T {
  const index = Math.trunc(Math.random() * models.length);
  return models[index];
}

function getRandomLatency(maxLatency = 3_000): number {
  return Math.trunc(Math.random() * maxLatency);
}

function getRandomAttemptCount(maxAttempts = 3): number {
  return Math.trunc(Math.random() * maxAttempts);
}

async function sleep(ms: number): Promise<void> {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export class LogGenerator {
  private logger?: Logger;
  private baseDelay: number = 100;
  private amountOfEntries: number = 5_000;

  public withBaseDelay(ms: number): LogGenerator {
    this.baseDelay = ms;
    return this;
  }

  public withLogger(logger: Logger): LogGenerator {
    this.logger = logger;
    return this;
  }

  public withAmountOfEntries(amount: number): LogGenerator {
    this.amountOfEntries = amount;
    return this;
  }

  public async generateRandomMetrics(): Promise<void> {
    if (!this.logger) {
      throw new Error(`Please provide a logger`);
    }

    const promises: Promise<void>[] = [];
    for (let i = 0; i < this.amountOfEntries; i++) {
      const metric: Metric = {
        Service: getRandomElementFrom(Services),
        Api: getRandomElementFrom(ApiCalls),
        Latency: getRandomLatency(Math.trunc(2_500 * Math.random())),
        AttemptCount: getRandomAttemptCount(),
        Params: getRandomElementFrom(Params)
      };
      promises.push(this.sleepAndLog(this.baseDelay, metric));
    }

    await Promise.all(promises);
    this.logger!.trace(`Finsihed ${this.amountOfEntries} entries`);
  }

  private async sleepAndLog(baseDelay: number, metric: Metric): Promise<void> {
    const delay = baseDelay * Math.random();
    await sleep(delay);
    this.logger!.metric(metric);
  }
}
