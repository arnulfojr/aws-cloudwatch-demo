#!/usr/bin/env node
import "source-map-support/register";
import * as cdk from "@aws-cdk/core";
import { AwsCloudwatchDemoStack } from "./stack";

const app = new cdk.App();

new AwsCloudwatchDemoStack(app, "AwsCloudwatchDemoStack");
