import * as path from "path";
import * as cdk from "@aws-cdk/core";
import * as lambda from "@aws-cdk/aws-lambda";
import * as events from "@aws-cdk/aws-events";
import * as targets from "@aws-cdk/aws-events-targets";
import * as logs from "@aws-cdk/aws-logs";

export class AwsCloudwatchDemoStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const codePath = path.join(__dirname, "../build", "LambdaFunction");
    const code = lambda.Code.fromAsset(codePath);

    const lambdaFunction = new lambda.Function(this, "LambdaFunction", {
      functionName: "CloudWatchDemoFunction",
      description: "Random lambda to generate logs",
      runtime: lambda.Runtime.NODEJS_12_X,
      handler: "lambda/app.handler",
      memorySize: 256,
      timeout: cdk.Duration.minutes(1),
      tracing: lambda.Tracing.ACTIVE,
      logRetention: logs.RetentionDays.TWO_WEEKS,
      code
    });

    const lambdaTarget = new targets.LambdaFunction(lambdaFunction);
    new events.Rule(this, "ScheduledRule", {
      schedule: events.Schedule.rate(cdk.Duration.minutes(1)),
      targets: [lambdaTarget]
    });
  }
}
